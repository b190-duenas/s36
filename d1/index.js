//Basic Imports
const express = require("express");
const mongoose = require("mongoose");
// allows us to use contents of "taskRoutes.js" in the "routes" folder
const taskRoutes = require("./routes/tasksRoutes.js");

//Server setup
const app = express();
const port = 3000;

//MongoDb Connection
mongoose.connect("mongodb+srv://renzrad:renzrad@wdc028-course-booking.uf7kzlq.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", ()=> console.log("We're connected to the database"));


app.use(express.json());
app.use(express.urlencoded({extended:true}));

// app.use that is used for setting routes for accessing different collections must be placed after the "app.use(express.json())" and "app.use(express.urlencoded({extended:true}))"  

// Allows all the task routes created in the "taskRoutes.js" files to use "/tasks" route
app.use("/tasks", taskRoutes);


// Server Running Confirmation
app.listen(port, ()=> console.log(`Server running at port: ${port}`));